Kelompok C14
Anggota: 
1.	1906399575	Adrian Chandra Rachman
2.	1906400186	Aimar Fikri Salafi
3.	1906399713	Erasto Akbar Adjie
4.	1906399612	Najla Laila Muharram
Link Herokuapp: https://tugaskelompok2ppw.herokuapp.com/

Website kita : Website kita merupakan website untuk menginformasikan Event-event yang diadakan oleh Universitas/Sekolah Menengah Atas (SMA), Tujuan untuk host sebagai wadah mencari pengunjung dan tujuan kedua untuk pengunjung adalah untuk mencari Event-event yang ingin ia kunjungi.


List Fitur:
1.	Page selection untuk host/attendees
2.	List Event yang sudah terdaftar
3.	Form untuk host
4.	Form untuk attendees
5.  Merchandise Event
6.  Comment Event
 
Pipeline:
[![pipelinestatus](https://gitlab.com/pepewin/tkppw/badges/master/pipeline.svg)](https://gitlab.com/pepewin/tkppw/commits/master)
[![coverage report](https://gitlab.com/pepewin/tkppw/badges/master/coverage.svg)](https://gitlab.com/pepewin/tkppw/commits/master)
