from django.urls import path
from . import views

app_name = 'CreateEvent'

urlpatterns = [
    path('', views.CreateEvent, name="indexCreate"),
    path('view/', views.ViewEvent, name="ViewEvent"),
    path('view/getEvents/', views.getEvents, name='getEvents'),

]