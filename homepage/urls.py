from django.urls import path
from django.contrib.auth import views as av
from .views import register
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name="home"),
    path('user-type/', views.user_type, name='user_type'),
    path('login/', av.LoginView.as_view(template_name='auth.html'), name='auth'),
    path('logout/', av.LogoutView.as_view(template_name='logout.html'), name='logout'),
    path('register/', register, name='register'),
]