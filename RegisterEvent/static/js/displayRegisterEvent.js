$.ajax({
    url:window.location.origin + "/RegisterEvent/json/" + id,
    contentType:"application/json",
    dataType:"json",
    success: function(response) {
        console.log(response);
        var obj_nama = $("#the-event");
        var obj_data = $("#target-data")
        var event = response.event;
        obj_nama.empty();
        obj_data.empty();
        
        obj_nama.append(event)
        
        for(i = 0; i < response.data.length; i++) {
            var name = response.data[i].fields.name;
            obj_data.append('<p class="card-text"><li>' + name + '</li></p>');
        }

    }
});

$("#the-event").dblclick(function() {
    window.location.replace("/CreateEvent/view/");
});