from django.db import models
from CreateEvent.models import Event
# Create your models here.

class Merch(models.Model):
    event = models.ForeignKey(Event,related_name="merchandise", on_delete=models.CASCADE)
    name = models.CharField( max_length=50, null = True)
    Price = models.CharField( max_length=50, null = True)
    Picture = models.ImageField(null = True, blank=True)

    def __str__(self):
        return self.name

  

 