from django.shortcuts import render,redirect
from .forms import CreateEventForm
from .models import Event
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy

from django.http import HttpResponse, JsonResponse
import json

# @login_required(login_url=reverse_lazy('homepage:auth'))
# def CreateEvent(request):
#     my_form = CreateEventForm()

#     if request.method == 'POST':
#         my_form = CreateEventForm(request.POST or None ,request.FILES or None)
        
#         if my_form.is_valid():
#             Event.objects.create(**my_form.cleaned_data)
#             return redirect("CreateEvent:indexCreate")

#     context = {
#         'form' : my_form,
#     }
#     return render(request, "indexCreateAjax.html", context)   

# Create your views here.
@login_required(login_url=reverse_lazy('homepage:auth'))
def CreateEvent(request):
    my_form = CreateEventForm()
    response = {}
    if request.method == 'POST':
        if request.is_ajax():
            my_form = CreateEventForm(request.POST or None ,request.FILES or None)
            
            if my_form.is_valid():
                Event.objects.create(**my_form.cleaned_data)
                return JsonResponse({'success':True})
        return JsonResponse({'success':False})

    context = {
        'form' : my_form,
    }
    return render(request, "indexCreate.html", context)  

# @login_required(login_url=reverse_lazy('homepage:auth'))
# def eventMerch(request):
#     if request.method == 'POST':
#         pic = request.FILES['Picture']
#         nama =  request.POST['eventName']
#         desc = request.POST['description']
#         date = request.POST['eventDate']
#         tempat = request.POST['tempat']
#         Merch.objects.create(
#         Picture = pic
#         eventName = nama,
#         description = desc,
#         eventDate = date ,
#         tempat = tempat
#         )
#         return HttpResponse('')
#     # context = { "id": id  }
#     return render(request, "indexCreate.html")

def ViewEvent(request):
    events = Event.objects.all()

    context = {
        'events' : events
    }
    return render(request, "listEvent.html", context)  

def getEvents(request):
    userReq = Event.objects.all().values()
    lst = list(userReq)
    #print(lst)
    return JsonResponse(lst, safe=False)


# def model_form_upload(request):
#     if request.method == 'POST':
#         form = DocumentForm(request.POST, request.FILES)
#         if form.is_valid():
#             form.save()
#             return redirect('home')
#     else:
#         form = DocumentForm()
#     return render(request, 'model_form_upload.html', {
#         'form': form
#     })
