from django.forms import ModelForm, TextInput, FileInput
from .models import Merch
from django.utils.translation import gettext_lazy as _

class MerchForm(ModelForm):
    class Meta:
        model = Merch
        fields = '__all__'
        exclude= ["event"]
        widgets = {
            "name": TextInput(attrs={"class":"form-control","placeholder": "Pensil", "required":True}),
            "Price": TextInput(attrs={"class":"form-control ","placeholder": "Rp. 1.000.000,00", "required":True}),
            "Picture": FileInput(attrs={"id":"imageinput","required":True})
        }