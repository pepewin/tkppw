$(function () {
    var infoArea = document.getElementById('capt');
    $("#imageinput").change(function (event) {
        var x = URL.createObjectURL(event.target.files[0]);
        var z = "url(" + x + ") center center no-repeat "
        infoArea.textContent = "";
        $(".form-pic").css({ "background": z, "background-size": "contain" });
        console.log(event);


    });
    $(".ml2").click(function(){

        // Wrap every letter in a span
     var textWrapper = document.querySelector('.ml2');
     textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

     anime.timeline({ loop: true })
         .add({
             targets: '.ml2 .letter',
             scale: [4, 1],
             opacity: [0, 1],
             translateZ: 0,
             easing: "easeOutExpo",
             duration: 950,
             delay: (el, i) => 70 * i
         }).add({
             targets: '.ml2',
             opacity: 0,
             duration: 1000,
             easing: "easeOutExpo",
             delay: 1000
         });
    })
    $(".banner").dblclick(function(){
        if($(".banner").hasClass("active")){
            $(".banner").removeClass("active")
        }
        else
        $(".banner").addClass("active");
    })
});