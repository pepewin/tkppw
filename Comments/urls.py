from django.urls import path
from . import views

app_name = 'Comments'

urlpatterns = [
    path('<int:id>/', views.comments, name='Comments'),
    path ('<int:id>/data/',views.comments_data, name='Comments_data'),
    path('display/<int:id>/', views.displayComments, name='DisplayComments'),
    path ('display/<int:id>/data/',views.displayComments_data, name='DisplayComments_data'),
]