import json
from django.shortcuts import render,redirect
from django.http.response import HttpResponse, JsonResponse
from .forms import CommentsForm
from .models import Comments
from django.contrib import messages
from CreateEvent.models import Event
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy

# def displayComments(request, id):
    
#     event = Event.objects.get(id=id)
#     commentsValue = Comments.objects.all()
#     context = {'event': event, 'commentsValue': commentsValue}

#     return render(request, 'Comments/displayComments.html', context)

def displayComments(request,id):
    event = Event.objects.get(id=id)
    context = {'event':event}
    return render(request, 'Comments/displayComments.html',context)

def displayComments_data(request,id):
    data =[]
    event = Event.objects.get(id=id)
    commentsValue = Comments.objects.all()
    for i in commentsValue :
        if i.event.eventName == event.eventName :
            temp = dict()
            temp['id'] = i.id
            temp['name'] = i.name
            temp["comment"] = i.comment 
            data.append(temp)

    context = {'data':data}
    return JsonResponse(context)

@login_required(login_url=reverse_lazy('homepage:auth'))
def comments(request,id):
    form = CommentsForm(request.POST or None)
    context = {'form':form}
    return render(request, 'Comments/index.html', context)

def comments_data(request,id):
    if request.method == 'POST':
        event = Event.objects.get(id=id)
        
        name = request.POST.get('name')
        comment = request.POST.get('comments')

        response_data = dict()
        
        # print(name)
        # print(comment)
        post = Comments(user = request.user,event=event,name=name,comment=comment)
        post.save()

        response_data['result'] = 'Create post successful'

        return JsonResponse(response_data)


              
# def comments(request, id):
#     form = CommentsForm(request.POST or None)
#     if form.is_valid():
#         obj = form.save(commit=False)
#         obj.event = Event.objects.get(id=id)
#         messages.success(request, f"Comment Sucesfully Added")
#         form.instance.event_id = id
#         form.instance.user = request.user
#         form.save()
#         return redirect("CreateEvent:ViewEvent")
#     context = {'form':form}
#     return render(request, 'Comments/index.html', context)


