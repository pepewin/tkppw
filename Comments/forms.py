from django.forms import ModelForm, TextInput
from .models import Comments


class CommentsForm(ModelForm):
    class Meta:
        model = Comments
        fields = ['name', 'comment']
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Eros','id':'name','required': True}),
            'comment': TextInput(attrs={'placeholder': 'Comment here','id':'comment','required': True})
        }
