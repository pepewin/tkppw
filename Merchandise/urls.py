from django.urls import path

from . import views

app_name = 'Merchandise'

urlpatterns = [
    path('RegisterMerch/<int:id>/', views.registerMerch, name='RegisterMerch'),
    path('ListMerch/<int:id>/', views.displayMerch, name='ListMerch'),
    path('ListMerch/json/<int:id>/', views.MerchApi, name='MerchJson'),
    path('RegisterMerchAjax/<int:id>/', views.ajaxMerch, name='RegisterMerchAjax'),

]