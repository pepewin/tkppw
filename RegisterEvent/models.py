from django.db import models
from CreateEvent.models import Event
from django.core.validators import MinValueValidator

# Create your models here.
class RegisterEvent(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    tickets = models.PositiveIntegerField(validators=[MinValueValidator(1)])

    def __str__(self):
        return self.name
