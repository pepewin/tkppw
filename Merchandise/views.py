from django.shortcuts import render,redirect
from .forms import MerchForm
from CreateEvent.models import Event
from .models import Merch
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.http import JsonResponse,HttpResponse
# Create your views here.
@login_required(login_url=reverse_lazy('homepage:auth'))
def registerMerch(request, id):
    form = MerchForm()
    if request.method == "POST":
        form = MerchForm(request.POST or None ,request.FILES or None)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.event = Event.objects.get(id=id)
            obj.save()
            return redirect ('/Merch/ListMerch/'+str(id)+'/')
    context = {'form':form,
                  }
    return render(request, 'RegisterMerch.html',context)

@login_required(login_url=reverse_lazy('homepage:auth'))
def ajaxMerch(request,id):
    if request.method == 'POST':
        event = Event.objects.get(id=id)
        pic = request.FILES['Picture']
        nama =  request.POST['name']
        price = request.POST['Price']
        Merch.objects.create(
        event = event,
        name = nama,
        Price = price,
        Picture = pic
        )
        return HttpResponse('')
    context = { "id": id  }
    return render(request,'registermerchajax.html',context)


def displayMerch(request, id):
    event = Event.objects.get(id=id)
    Merch_list = event.merchandise.all()
    context = { 'event':event,'Merch_list':Merch_list  ,"id": id  }
    return render(request, 'ListMerch.html',context)

def MerchApi(request,id):
    event = Event.objects.get(id=id)
    Merch_list = event.merchandise.all()
    data ={"items":[]}
    for merch in Merch_list:
        pic = merch.Picture.url
        name = merch.name
        price = merch.Price
        data["items"].append({'pic':pic,'name':name,'price':price})
    return JsonResponse(data)
    