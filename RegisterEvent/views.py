from django.shortcuts import render
from .forms import RegisterEventForm
from .models import RegisterEvent
from CreateEvent.models import Event
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.core import serializers
from django.http.response import JsonResponse
import json

# Create your views here.
@login_required(login_url=reverse_lazy('homepage:auth'))
def registerEvent(request, id):
    form = RegisterEventForm()
    if request.method == "POST":
        form = RegisterEventForm(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.event = Event.objects.get(id=id)
            obj.save()

    context = {"form": form}
    return render(request, "RegisterEvent/index.html", context)


def displayRegisterEvent(request, id):
    context = {"id": id}
    return render(request, "RegisterEvent/displayRegisterEvent.html", context)


def displayJson(request, id):
    event = Event.objects.get(id=id)
    registerevent = RegisterEvent.objects.filter(event=event)
    data = serializers.serialize("json", registerevent)
    data = json.loads(data)
    context = {'event': str(event), 'data': data}
    return JsonResponse(context, json_dumps_params={'indent': 2}, safe=False)
